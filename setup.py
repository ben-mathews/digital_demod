#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages

setup(name='digital_demod',
      version='0.0.1',
      description='Python 3 digital signal demodulation routines',
      author="Ben Mathews",
      author_email="ben@nothing.com",
      url="https://gitlab.com/ben-mathews/digital_demod",
      install_requires=[
          "scipy>=1.2.1",
          "numpy>=1.16.3",
          "matplotlib>=3.0.3",
      ],
      packages=['digital_demod', 'digital_demod.adaptive_filtering', 'digital_demod.carrier_recovery', 'digital_demod.clock_recovery', 'digital_demod.measurements', 'digital_demod.signal_utilities', 'digital_demod.utilities']
     )
