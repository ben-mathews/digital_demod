import numpy as np
import matplotlib.pyplot as plt

def carrier_phase_recovery(iq, mod_order, debug_plots):
    """
    Compute average phase angle of IQ data

    :param numpy.ndarray iq: IQ data to process
    :param int mod_order: Constellation order (BPSK=2, QPSK=4, 8PSK=8)
    :param bool debug_plots: Control debug plots

    :return: float Detected phase angle in radians
    """
    if np.mean(np.abs((np.angle(iq ** mod_order)))) > np.pi/2:
        phase_data = np.angle((np.exp(1j*np.pi))*(iq ** mod_order))
        adjustment = -np.pi
    else:
        phase_data = np.angle(iq ** mod_order)
        adjustment = 0.0

    if debug_plots:
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(phase_data, '.')
        plt.xlabel('Sample Number')
        plt.ylabel('Phase (rad)')
        plt.grid()
        plt.title('Carrier Phase Estimation')

    return (np.mean(phase_data) + adjustment) / mod_order

def carrier_phase_shift(iq, phase_rad, t_sec=None):
    """

    :param numpy.ndarray iq: IQ data to process
    :param phase_rad: Phase angle to shift data by

    :return numpy.ndarray: Phase-shifted IQ data
    """
    iq = iq * np.exp(1j * phase_rad)
    return iq