import numpy as np
from scipy import stats
from scipy.signal import welch
import matplotlib.pyplot as plt

def carrier_recovery_coarse(iq, fs_hz, max_offset = 0.2, NFFT=8192, percent_total_power_bandwidth = 0.975, debug_plots=False):
    f_welch, s_welch = welch(iq, fs_hz, 'hann', 1024, return_onesided=False, detrend=False)
    f_welch = np.fft.fftshift(f_welch)
    s_welch = np.fft.fftshift(np.abs(s_welch))
    center_freq_index = np.argmin(np.abs(np.cumsum(s_welch)-np.sum(s_welch)/2))
    frequency_min_hz = f_welch[center_freq_index]

    s_welch_half_power = np.roll(s_welch, int(len(s_welch)/2 - center_freq_index))
    half_power_vector = np.abs(s_welch_half_power[int(len(s_welch_half_power) / 2)::] + s_welch_half_power[0:int(len(s_welch_half_power) / 2)][::-1])
    total_power = np.sum(half_power_vector)
    bandwidth_estimate_hz = 2*np.abs(f_welch[int(len(f_welch)/2) + np.argmin(np.abs(np.cumsum(np.abs(half_power_vector))-percent_total_power_bandwidth*total_power))])

    if False:
        import matplotlib.patches as patches
        fig, ax = plt.subplots(1)
        plt.plot(f_welch, 10 * np.log10(s_welch))
        plt.grid()
        rect = patches.Rectangle([-1.0 * bandwidth_estimate_hz / 2.0 + frequency_min_hz, 10 * np.min(np.log10(np.abs(s_welch))) - 10],
                                 bandwidth_estimate_hz, 100, linewidth=1, edgecolor='none', facecolor='y', alpha=0.5)
        ax.add_patch(rect)

    do_old_way = False
    if do_old_way:
        frequency_search_hz = np.linspace(-fs_hz / 2, fs_hz / 2, NFFT, NFFT)
        iq_spectra = np.fft.fftshift(np.fft.fft(iq, NFFT))
        frequency_min_hz = frequency_search_hz[np.argmin(np.abs(np.cumsum(np.abs(iq_spectra))-np.sum(np.abs(iq_spectra))/2))]

        half_power_vector = np.abs(iq_spectra[int(len(iq_spectra) / 2)::] + iq_spectra[0:int(len(iq_spectra) / 2)][::-1])
        total_power = np.sum(half_power_vector)
        percent_total_power = 0.975 # Percentage of total power is occupied carrier BW (0.0 - 1.0)
        bandwidth_estimate_hz = 2*np.abs(frequency_search_hz[int(len(frequency_search_hz)/2) + np.argmin(np.abs(np.cumsum(np.abs(half_power_vector))-percent_total_power*total_power))])
        if False:
            import matplotlib.patches as patches
            fig, ax = plt.subplots(1)
            plt.plot(frequency_search_hz, 10 * np.log10(np.abs(iq_spectra)))
            plt.grid()
            rect = patches.Rectangle( [-1.0 * bandwidth_estimate_hz / 2.0, 10*np.min(np.log10(np.abs(iq_spectra))) - 10],
                bandwidth_estimate_hz, 100, linewidth=1, edgecolor='none', facecolor='y', alpha=0.5)
            ax.add_patch(rect)
    return frequency_min_hz, bandwidth_estimate_hz


def carrier_recovery_mfd(iq, mod_order, fs_hz = 1.0, max_offset_hz = 0.1, debug_plots=False):
    """
    Compute carrier frequency offset by the multiply-filter-divide method

    :param iq: numpy.ndarray IQ data to process
    :param int mod_order: Constellation order (BPSK=2, QPSK=4, 8PSK=8)
    :param float fs_hz: Sampling rate in Hz
    :param float max_offset_hz: Max frequency offset to search for
    :param bool debug_plots: Control debug plots

    :return: float Detected carrier frequency offset
    """
    frequency_offset_hz, metric_snr = carrier_recovery_mfd_coarse_search(iq, mod_order, fs_hz, max_offset_hz, 1, debug_plots)

    return frequency_offset_hz, metric_snr


def carrier_recovery_mfd_coarse_search(iq, mod_order, fs_hz, max_offset_hz, zero_stuffing_factor=16, debug_plots=False):
    """
    Compute carrier frequency offset by the multiply-filter-divide method

    :param iq: numpy.ndarray IQ data to process
    :param int mod_order: Constellation order (BPSK=2, QPSK=4, 8PSK=8)
    :param float fs_hz: Sampling rate in Hz
    :param float max_offset_hz: Max frequency offset to search for
    :param bool debug_plots: Control debug plots

    :return: float Detected carrier frequency offset
    """
    # Create carrier spectra
    iq = np.append(iq, np.zeros((zero_stuffing_factor-1)*len(iq))) # Zero-pad for additional resolution
    NFFT = len(iq)
    frequency_search_hz = fs_hz * np.fft.fftshift(np.fft.fftfreq(NFFT))
    iq_power = iq ** mod_order
    iq_spectra = np.fft.fft(iq_power)
    iq_spectra = np.fft.fftshift(iq_spectra)

    # Only process +/- max_offset_hz
    iq_spectra = iq_spectra[abs(frequency_search_hz / mod_order) < max_offset_hz]
    frequency_search_hz = frequency_search_hz[abs(frequency_search_hz / mod_order) < max_offset_hz]

    # Compute
    iq_spectra_avg = np.convolve(np.abs(iq_spectra), np.ones((3)) / 3.0, mode='same')

    # Compute fit
    index_max = np.argmax(iq_spectra_avg)
    (index_fit, peakval) = parabolic(np.abs(iq_spectra), index_max)
    metric_snr = peakval / np.median(np.abs(iq_spectra))
    frequency_offset_hz = (frequency_search_hz[index_max] + (index_fit - index_max)/fs_hz/NFFT) / mod_order

    if False:
        X = iq_spectra
        index_max = np.argmax(np.abs(iq_spectra))
        k = int(index_max)
        # print(np.abs([X[k-1], X[k], X[k+1]]))
        def tau(x):
            return 1/4 * np.log(3*x**2 + 6*x + 1) - np.sqrt(6)/24 * np.log((x + 1 - np.sqrt(2/3)) / (x + 1 + np.sqrt(2/3)))
        ap = (np.real(X[k+1]) * np.real(X[k]) + np.imag(X[k+1]) * np.imag(X[k])) / (np.real(X[k]) * np.real(X[k]) + np.imag(X[k]) * np.imag(X[k]))
        dp = -ap / (1-ap)
        am = (np.real(X[k-1]) * np.real(X[k]) + np.imag(X[k-1]) * np.imag(X[k])) / (np.real(X[k]) * np.real(X[k]) + np.imag(X[k]) * np.imag(X[k]))
        dm = am / (1-am)
        delta_index_max = (dp + dm) / 2 + tau(dp * dp)-tau(dm * dm)
        frequency_offset_hz = (frequency_search_hz[index_max] + delta_index_max * (fs_hz / NFFT)) / mod_order

    # Try: Fast, Accurate Frequency Estimators (http://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.555.2873&rep=rep1&type=pdf)
    # Also see: http://users.metu.edu.tr/ccandan//pub_dir/FineDopplerEst_IEEE_SPL_June2011.pdf
    if False:
        index_max = np.argmax(np.abs(iq_spectra_avg))
        delta_index_max = -1.0 * np.real((iq_spectra_avg[index_max + 1] - iq_spectra_avg[index_max - 1]) / (2 * iq_spectra_avg[index_max] - iq_spectra_avg[index_max - 1] - iq_spectra_avg[index_max + 1]))
        frequency_offset_hz = (frequency_search_hz[index_max] + delta_index_max * (fs_hz / NFFT)) / mod_order
    index_max = np.argmax(np.abs(iq_spectra))
    if index_max == 0:
        delta_index_max = 0
    elif index_max == len(iq_spectra)-1:
        delta_index_max = len(iq_spectra) - 1
    else:
        delta_index_max = -1.0 * np.real((iq_spectra[index_max + 1] - iq_spectra[index_max - 1]) / (2 * iq_spectra[index_max] - iq_spectra[index_max - 1] - iq_spectra[index_max + 1]))
    frequency_offset_hz = (frequency_search_hz[index_max] + delta_index_max * (fs_hz / NFFT)) / mod_order
    (index_fit, peakval) = parabolic(np.abs(iq_spectra), index_max)
    metric_snr = 10*np.log10(peakval / np.median(np.abs(iq_spectra)))

    # Debug plotting
    if debug_plots:
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(frequency_search_hz / mod_order, 10 * np.log10(np.abs(iq_spectra)), label='Raw')
        plt.plot(frequency_search_hz / mod_order, 10 * np.log10(np.abs(iq_spectra_avg)), label='Averaged')
        plt.plot(frequency_offset_hz, np.max((np.max(10 * np.log10(np.abs(iq_spectra))), np.max(10 * np.log10(np.abs(iq_spectra_avg))))), 'x', label='Peak')

        plt.figure(figsize=(7.0, 4.75))
        plt.plot(frequency_search_hz / mod_order, 10 * (np.abs(iq_spectra)), label='Raw')
        plt.plot(frequency_search_hz / mod_order, 10 * (np.abs(iq_spectra_avg)), label='Averaged')
        plt.plot(frequency_offset_hz, np.max((np.max(10 * (np.abs(iq_spectra))), np.max(10 * np.log10(np.abs(iq_spectra_avg))))), 'x', label='Peak')

        plt.grid(True)
        plt.legend()
        plt.xlabel('Frequency Offset (Hz)')
        plt.ylabel('Power (dB)')
        plt.title('Carrier Frequency Offset Detection Results')

    if False:
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(frequency_search_hz / mod_order, np.angle(iq_spectra), label='Raw')
        plt.plot(frequency_search_hz[index_max] / mod_order, np.angle(iq_spectra[index_max]), 'x', label='Peak')
        plt.grid(True)
        plt.legend()
        plt.xlabel('Frequency Offset (Hz)')
        plt.ylabel('Power (dB)')
        plt.title('Carrier Phase Offset Detection Results')

    return frequency_offset_hz, metric_snr


def carrier_recovery_m_fine(iq, mod_order, t_sec, debug_plots):
    """
    Compute fine carrier frequency offset by doing a first-order fit to phase measurements

    :param numpy.ndarray iq: IQ data to process
    :param int mod_order: Constellation order (BPSK=2, QPSK=4, 8PSK=8)
    :param numpy.ndarray t_sec: Vector of time values associated with the IQ data
    :param bool debug_plots: Control debug plots

    :return: float Detected carrier frequency offset
    """

    if np.mean(np.abs((np.angle(iq ** mod_order)))) > np.pi/2: # If our phase measurements are not centered near 0, offset them so we don't have problems with wrapping
        phase_data = np.angle((np.exp(1j*np.pi))*(iq ** mod_order))
        adjustment = -np.pi
    else:
        phase_data = np.angle(iq ** mod_order)
        adjustment = 0.0
    slope, intercept, r_value, p_value, std_err = stats.linregress(t_sec, phase_data)

    if debug_plots:
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(t_sec, phase_data, '.', label='Raw Phase Data')
        plt.plot(t_sec, intercept + t_sec*slope, label='Fitted Data')
        plt.xlabel('Time (sec)')
        plt.ylabel('Phase (rad)')
        plt.legend()
        plt.grid()
        plt.title('Fine Carrier Frequency Estimation')

    frequency_offset_hz = slope / (mod_order * 2 * np.pi)
    return frequency_offset_hz, intercept+adjustment


def carrier_frequency_shift(iq, frequency_hz, t_sec=None, fs_hz=None, inplace=False):
    """
    Compute fine carrier frequency offset by doing a first-order fit to phase measurements

    :param numpy.ndarray iq: IQ data to process
    :param float frequency_hz: Frequency to shift the IQ data by
    :param t_sec: Vector of time values associated with the IQ data

    :return: numpy.ndarray: Frequency shifted IQ data
    """
    if t_sec is None:
        t_sec = np.linspace(0, len(iq)-1, len(iq)) / fs_hz
    complex_exponential = np.exp(1j * 2 * np.pi * frequency_hz * t_sec)
    if inplace:
        iq *= complex_exponential
    else:
        iq = iq * complex_exponential
    return iq



def parabolic(f, x):
    """Quadratic interpolation for estimating the true position of an
    inter-sample maximum when nearby samples are known.

    f is a vector and x is an index for that vector.

    Returns (vx, vy), the coordinates of the vertex of a parabola that goes
    through point x and its two neighbors.

    Example:
    Defining a vector f with a local maximum at index 3 (= 6), find local
    maximum if points 2, 3, and 4 actually defined a parabola.

    In [3]: f = [2, 3, 1, 6, 4, 2, 3, 1]

    In [4]: parabolic(f, argmax(f))
    Out[4]: (3.2142857142857144, 6.1607142857142856)

    """
    # Requires real division.  Insert float() somewhere to force it?
    xv = 1 / 2 * (f[x - 1] - f[x + 1]) / (f[x - 1] - 2 * f[x] + f[x + 1]) + x
    yv = f[x] - 1 / 4 * (f[x - 1] - f[x + 1]) * (xv - x)

    return (xv, yv)


