import copy
import numpy as np
from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_recovery_mfd, carrier_frequency_shift
from digital_demod.measurements.freq_domain_measurements import basic_freq_domain_analysis

def mod_rec_psk(iq, t_sec, fs_hz, max_offset_hz, mod_orders=[2, 4, 8], debug_plots=False):
    metric_snrs = []

    iq_ = copy.deepcopy(iq)

    center_frequency_hz, carrier_bandwidth_hz, cn_db = basic_freq_domain_analysis(iq=iq_, fs_hz=fs_hz, usable_ibw_fraction=0.8, fraction_power_carrier=0.99, debug_plots=debug_plots)
    iq_ = carrier_frequency_shift(iq_, center_frequency_hz, t_sec)

    for idx, mod_order in enumerate(mod_orders):
        frequency_offset_hz, metric_snr = carrier_recovery_mfd(iq_, mod_order, fs_hz = fs_hz, max_offset_hz = max_offset_hz, debug_plots=debug_plots)
        metric_snrs.append(metric_snr)

    mod_order = mod_orders[np.argmax(metric_snrs)]
    return mod_order
