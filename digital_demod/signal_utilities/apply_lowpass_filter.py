import numpy as np
import matplotlib.pyplot as plt
from scipy import signal
from scipy.signal import filtfilt

def apply_lowpass_filter(iq, fs_hz, pb_freq_hz, debug_plot=False):
    numtaps = 271
    b = signal.firwin(numtaps, cutoff=2 * pb_freq_hz / fs_hz, window="hamming")
    if debug_plot:
        mfreqz(b)
    iq = filtfilt(b, [1], iq)
    return iq


def mfreqz(b,a=1):
    from scipy import signal
    w,h = signal.freqz(b,a)
    h_dB = 20 * np.log10 (np.abs(h))
    plt.figure()
    plt.subplot(211)
    plt.plot(w/max(w),h_dB)
    plt.ylim(-150, 5)
    plt.ylabel('Magnitude (db)')
    plt.xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    plt.title(r'Frequency response')
    plt.subplot(212)
    h_Phase = np.unwrap(np.arctan2(np.imag(h),np.real(h)))
    plt.plot(w/max(w),h_Phase)
    plt.ylabel('Phase (radians)')
    plt.xlabel(r'Normalized Frequency (x$\pi$rad/sample)')
    plt.title(r'Phase response')
    plt.subplots_adjust(hspace=0.5)