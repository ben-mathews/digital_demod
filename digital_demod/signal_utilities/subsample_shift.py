import numpy as np
from scipy.signal import lfilter, filtfilt, convolve

def subsample_shift(iq, delay_samples):
    N = 201  # Filter length.
    n = np.arange(N)

    # Compute sinc filter.
    h = np.sinc(n - (N - 1) / 2 - delay_samples)

    # Multiply sinc filter by window
    h *= np.blackman(N)

    # Normalize to get unity gain.
    h /= np.sum(h)

    #iq_resampled = lfilter(h, 1, iq)
    iq_resampled = convolve(h, iq)
    iq_resampled = iq_resampled[int(np.floor(N/2)):-int(np.floor(N/2)):]

    return iq_resampled
