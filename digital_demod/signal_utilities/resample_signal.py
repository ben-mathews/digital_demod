import fractions
from scipy.signal import resample_poly
def resample_signal(iq, fs_hz, new_fs_hz):
    """
    Resample IQ data

    :param numpy.ndarray iq: IQ data to process
    :param float fs_hz: Original sample rate in Hz
    :param float new_fs_hz: New sample rate in Hz

    :return numpy.ndarray: Resampled IQ data
    """
    resample_frequency_frac = fractions.Fraction(new_fs_hz/fs_hz).limit_denominator(10000)
    iq_resampled = resample_poly(iq, resample_frequency_frac.numerator, resample_frequency_frac.denominator)
    return iq_resampled