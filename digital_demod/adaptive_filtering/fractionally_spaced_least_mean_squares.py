import numpy as np
import copy

def fractionally_spaced_least_mean_squares(u, d, step, w_initial, k):
    """
    Fractionally-space Least Mean Squares Equalizer

    :param numpy.ndarray u: Filter input (1 x n_input)
    :param numpy.ndarray d: Desired signal (1 x n_output)
    :param float step: Step size
    :param numpy.ndarray w_initial: Initial filter coefficients  (1 x n_filter)
    :param int k: Oversample factor (number of samples per symbol)

    :returns:
        - numpy.ndarray y: Filter output (1 x n_output)
        - numpy.ndarray e: Error signal (1 x n_output)
        - numpy.ndarray w: Matrix of filters  (n_filter x (n_output+1))
    """

    # Get lengths
    n_filter = int(len(w_initial))
    n_input = len(u)
    n_output = int((n_input-n_filter)/k+1)

    # Pre-allocate return matricies
    y = np.zeros(n_output)+1j*np.zeros(n_output)  # Filter output
    e = np.zeros(n_output)+1j*np.zeros(n_output)  # Error signal
    w = np.zeros((n_filter, n_output+1)) + 1j*np.zeros((n_filter, n_output+1))
    w[:, 0] = w_initial

    # Run the filter
    output_index = 0
    for n in range(n_input-n_filter+1):
        x = np.flipud(u[n:n+n_filter]) # Flip the x vector for convolution
        y_temp = np.dot(w[:, output_index], x) # Filter output
        if np.mod(n, k) == 0:
            y[output_index] = y_temp
            e[output_index] = d[output_index] - y_temp
            output_index = output_index + 1
            w[:,output_index] = w[:,output_index-1] + step * np.conj(x) * e[output_index-1]

    return y, e, w

