import numpy as np

def esn0(symbols_measured, symbols_truth):
    es_n0_dB = 10*np.log10(np.mean(np.abs(symbols_truth)) / np.var(symbols_measured - symbols_truth))
    return es_n0_dB

def symbol_slicer(symbols, constellation):
    symbol_indicies = []
    for symbol in symbols:
        symbol_indicies.append(np.argmin(np.abs(symbol - constellation)))
    return symbol_indicies
