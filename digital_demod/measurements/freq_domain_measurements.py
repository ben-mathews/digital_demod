import numpy as np
from scipy.signal import welch
import matplotlib.pyplot as plt


def basic_freq_domain_analysis(iq, fs_hz, usable_ibw_fraction, fraction_power_carrier=0.99, fraction_power_noise = 0.01, debug_plots=False):
    NFFT = 512
    f_welch, s_welch = welch(iq, fs_hz, 'hann', NFFT, return_onesided=False, detrend=False)
    f_welch = np.fft.fftshift(f_welch)
    s_welch = np.fft.fftshift(s_welch)
    if debug_plots:
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(f_welch, 10 * np.log10(s_welch), label='All Bandwidth')
    s_welch = s_welch[(f_welch > -fs_hz * usable_ibw_fraction / 2) & (f_welch < fs_hz * usable_ibw_fraction / 2)]
    f_welch = f_welch[(f_welch > -fs_hz * usable_ibw_fraction / 2) & (f_welch < fs_hz * usable_ibw_fraction / 2)]
    if debug_plots:
        plt.plot(f_welch, 10 * np.log10(s_welch), label='Usable Bandwidth')
    s_welch_cum_sum = np.cumsum(s_welch)
    carrier_indicies = (s_welch_cum_sum > (1.0 - fraction_power_carrier) / 2 * s_welch_cum_sum[-1]) & (s_welch_cum_sum < (fraction_power_carrier + (1.0 - fraction_power_carrier) / 2) * s_welch_cum_sum[-1])
    noise_indicies_low = (s_welch_cum_sum < fraction_power_noise / 2 * s_welch_cum_sum[-1])
    noise_indicies_high = (s_welch_cum_sum > (1.0 - fraction_power_noise / 2) * s_welch_cum_sum[-1])
    carrier_power_level = np.median(s_welch[carrier_indicies])
    noise_power_level = np.median(np.append(s_welch[noise_indicies_low], s_welch[noise_indicies_high]))
    center_frequency_hz = np.mean(f_welch[carrier_indicies])
    bandwidth_hz = np.max(f_welch[carrier_indicies]) - np.min(f_welch[carrier_indicies])
    cn_db = 10*np.log10( carrier_power_level / noise_power_level-  1 )
    if debug_plots:
        plt.plot(f_welch[carrier_indicies], 10 * np.log10(s_welch[carrier_indicies]), label='Detected Carrier BW')
        plt.plot(np.append(np.append(f_welch[noise_indicies_low], 0.0), f_welch[noise_indicies_high]), 10 * np.log10(np.append(np.append(s_welch[noise_indicies_low], np.nan), s_welch[noise_indicies_high])), label='Detected Noise BW')
        plt.plot(f_welch, 10 * np.log10(carrier_power_level * np.ones(len(f_welch))), label='Detected Carrier Power Level')
        plt.plot(f_welch, 10 * np.log10(noise_power_level * np.ones(len(f_welch))), label='Detected Noise Power Level')
        plt.grid()
        plt.legend()
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Power (dB)')
        plt.title('Carrier Bandwidth Estimation')

    return center_frequency_hz, bandwidth_hz, cn_db
