import numpy as np
import scipy.signal as signal
from copy import deepcopy

from digital_demod.clock_recovery.clock_tone_detect import clock_tone_detect
from digital_demod.signal_utilities.resample_signal import resample_signal
from digital_demod.signal_utilities.subsample_shift import subsample_shift

def clock_recovery(iq: np.ndarray, fs_hz: float, fsym_estimate_hz=None, debug_plots=False, oversample_by_2=False):
    """
    Detects symbol clock frequency, phase, and needed subsample shift

    :param iq: np.ndarray of IQ data
    :param fs_hz: Sample rate of iq in hz
    :param fsym_estimate_hz: Estimate of symbol rate (used to determine if we need to fold fsym_hz around fs_hz)
    :return:
     - fsym_hz: The detected symbol rate in Hz
     - fractional_sample_shift: The fractional sample shift to apply to iq to correct for symbol phase
     - clock_phase_offset_rad: The symbol phase in resampled data
    """
    if oversample_by_2:
        iq_oversampled = signal.resample_poly(iq, 2, 1)
        fs_oversampled_hz = 2 * fs_hz
    else:
        iq_oversampled = iq
        fs_oversampled_hz = fs_hz

    if False:
        num_samples_for_clock_tone_estimation = int(2 ** np.floor(np.log2(len(iq_oversampled) / 2)))
    else:
        num_samples_for_clock_tone_estimation = len(iq_oversampled)

    # Clock tone detect - This is a coarse estimate
    clock_tone_search_range_hz = (0.025 * fs_oversampled_hz, 0.49 * fs_oversampled_hz)
    clock_frequency_offset_hz, clock_phase_offset_rad = \
        clock_tone_detect(iq=iq_oversampled[0:num_samples_for_clock_tone_estimation], fs_hz=fs_oversampled_hz,
                          clock_tone_search_range_hz=clock_tone_search_range_hz, debug_plots=debug_plots)

    if fsym_estimate_hz is not None:
        if fsym_estimate_hz > fs_hz / 2:
            fsym_hz = fs_hz - clock_frequency_offset_hz
            clock_phase_offset_rad = -1.0 * clock_phase_offset_rad
        else:
            fsym_hz = clock_frequency_offset_hz
    else:
        fsym_hz = fs_hz - clock_frequency_offset_hz
    fractional_sample_shift = -1.0 * clock_phase_offset_rad / (np.pi * 2) * fs_hz / fsym_hz

    return fsym_hz, fractional_sample_shift, clock_phase_offset_rad


def resample_iq(iq: np.ndarray, fs_hz: float, fractional_sample_shift: float, fs_resampled_hz: float):
    """
    Resamples IQ to a specified resampling rate.  Typically called after clock_recovery() to resample to N x Fs
    prior to generating symbols.
    :param iq: np.ndarray of IQ data
    :param fs_hz: Sample rate of iq in hz
    :param fractional_sample_shift: The fractional sample shift to apply to iq to correct for symbol phase
    :param fs_resampled_hz: The sample rate of the resampled IQ data
    :return:
     - iq_resampled: Resampled IQ data
     - t_iq_resampled_sec: Time vector for resampled IQ data
    """
    iq_ = subsample_shift(iq, fractional_sample_shift)
    # Resample to oversample_factor x the symbol rate
    iq_resampled = resample_signal(iq_, fs_hz, fs_resampled_hz)
    t_iq_resampled_sec = -1 * fractional_sample_shift / fs_hz \
                         + np.linspace(0, len(iq_resampled) - 1, len(iq_resampled)) / fs_resampled_hz
    return iq_resampled, t_iq_resampled_sec


def clock_recovery_and_resample(iq: np.ndarray, fs_hz: float, fsym_initial_estimate_hz: float,
                                t_start_sec: float, oversample_by_2:bool = False, oversample_factor: float = 2):
    """
    Performs clock recovery and resampling of data to 2 x the detected symbol rate

    :param iq: np.ndarray of IQ data
    :param fs_hz: Sample rate of iq in hz
    :param fsym_initial_estimate_hz: Initial estimate of symbol rate
    :param t_start_sec: Starting time of IQ data
    :return:
     - fs_resampled_hz: The sample rate of the resampled IQ data
     - fsym_hz: The detected symbol rate in Hz
     - fractional_sample_shift: The fractional sample shift to apply to iq to correct for symbol phase
     - clock_phase_offset_rad: The symbol phase in resampled data
    """
    fsym_hz, fractional_sample_shift, clock_phase_offset_rad = \
        clock_recovery(iq=iq, fs_hz=fs_hz, fsym_estimate_hz=fsym_initial_estimate_hz, oversample_by_2=oversample_by_2)
    fs_resampled_hz = oversample_factor * fsym_hz
    iq_resampled, t_iq_resampled_sec = \
        resample_iq(iq, fs_hz, fractional_sample_shift, fs_resampled_hz)
    t_iq_resampled_sec += t_start_sec
    return fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec


def clock_recovery_and_generate_symbols(iq: np.ndarray, fs_hz: float, fsym_initial_estimate_hz: float,
                                t_start_sec: float, rrc_beta: float):
    """
    Performs clock recovery and resampling of data to 2 x the detected symbol rate

    :param iq: np.ndarray of IQ data
    :param fs_hz: Sample rate of iq in hz
    :param fsym_initial_estimate_hz: Initial estimate of symbol rate
    :param t_start_sec: Starting time of IQ data
    :param rrc_beta: RRC filter beta
    :return:
     - fs_resampled_hz: The sample rate of the resampled IQ data
     - fsym_hz: The detected symbol rate in Hz
     - fractional_sample_shift: The fractional sample shift to apply to iq to correct for symbol phase
     - clock_phase_offset_rad: The symbol phase in resampled data
     - symbols
      - t_symbols_sec
      - iq_resampled_rrc_filtered
      - oversample_factor
    """
    fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec = \
        clock_recovery_and_resample(iq=iq, fs_hz=fs_hz, fsym_initial_estimate_hz=fsym_initial_estimate_hz,
                                t_start_sec=t_start_sec)

    symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor = \
        generate_symbols(iq_resampled=iq_resampled, t_iq_resampled_sec=t_iq_resampled_sec,
                         fs_resampled_hz=fs_resampled_hz, fsym_hz=fsym_hz, rrc_beta=rrc_beta)

    return fsym_hz, fs_resampled_hz, iq_resampled, t_iq_resampled_sec, symbols, t_symbols_sec, oversample_factor


def generate_symbols(iq_resampled: np.ndarray, t_iq_resampled_sec: np.ndarray, fs_resampled_hz: float,
                     fsym_hz: float, rrc_beta: float):
    """
    Performs RRC filtering and decimation of oversampled IQ samples to generate symbols
    :param iq_resampled: Resampled IQ
    :param t_iq_resampled_sec: Time vector for iq_resampled
    :param fs_resampled_hz: Sample rate of resampled IQ
    :param fsym_hz: Symbol rate to generate symbols at
    :param rrc_beta: RRC filter beta
    :return:
      - symbols
      - t_symbols_sec
      - iq_resampled_rrc_filtered
      - oversample_factor
    """
    oversample_factor = int(np.round(fs_resampled_hz / fsym_hz))
    rrc_length = 64 * oversample_factor
    h_rrc, time_idx = rrc_filter(rrc_length, rrc_beta, fsym_hz, fs_resampled_hz)
    h_rrc = h_rrc / np.sum(h_rrc)
    # RRC filter and remove transients
    iq_resampled_rrc_filtered = np.convolve(iq_resampled, h_rrc)
    iq_resampled_rrc_filtered = \
        iq_resampled_rrc_filtered[int(np.floor(rrc_length / 2)):-(int(np.floor(rrc_length / 2 - 1)))]
    symbols = deepcopy(iq_resampled_rrc_filtered[0::oversample_factor])
    t_symbols_sec = deepcopy(t_iq_resampled_sec[0::oversample_factor])
    return symbols, t_symbols_sec, iq_resampled_rrc_filtered, oversample_factor


def rrc_filter(ntaps, rolloff, fsym_hz, fs_hz):
    """
    Design a RRC filter

    :param int ntaps: Number of filter taps
    :param float rolloff: Filter roll-off (e.g. 0.35)
    :param float fsym_hz: Symbol rate in Hz
    :param float fs_hz: Sample rate in Hz

    :return numpy.ndarray: Filter taps
    """
    time_idx = ((np.arange(ntaps) - ntaps / 2)) / float(fs_hz)
    sample_num = np.arange(ntaps)
    h_rrc = np.zeros(ntaps, dtype=float)

    for x in sample_num:
        t = (x - ntaps / 2) / float(fs_hz)
        if t == 0.0:
            h_rrc[x] = 1.0 - rolloff + (4 * rolloff / np.pi)
        elif rolloff != 0 and t == (1 / fsym_hz) / (4 * rolloff):
            h_rrc[x] = (rolloff / np.sqrt(2)) * (((1 + 2 / np.pi) * \
                                                  (np.sin(np.pi / (4 * rolloff)))) + (
                                                             (1 - 2 / np.pi) * (np.cos(np.pi / (4 * rolloff)))))
        elif rolloff != 0 and np.isclose(np.abs(t), np.abs((1 / fsym_hz) / (4 * rolloff))):
            h_rrc[x] = (rolloff / np.sqrt(2)) * (((1 + 2 / np.pi) * \
                                                  (np.sin(np.pi / (4 * rolloff)))) + (
                                                             (1 - 2 / np.pi) * (np.cos(np.pi / (4 * rolloff)))))
        else:
            Ts = 1 / fsym_hz
            B = rolloff
            h_rrc[x] = (np.sin(np.pi * (t / Ts) * (1 - B)) + (4 * B * (t / Ts)) * np.cos(np.pi * (t / Ts) * (1 + B))) \
                       / (np.pi * (t / Ts) * (1 - (4 * B * (t / Ts)) ** 2))
            h_rrc[x] = (np.sin(np.pi * t * (1 - rolloff) / (1 / fsym_hz)) + \
                        4 * rolloff * (t / (1 / fsym_hz)) * np.cos(np.pi * t * (1 + rolloff) / (1 / fsym_hz))) / \
                       (np.pi * t * (1 - (4 * rolloff * t / (1 / fsym_hz)) ** 2) / (1 / fsym_hz))

    return h_rrc, time_idx