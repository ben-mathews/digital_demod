import numpy as np
from scipy import stats
import matplotlib.pyplot as plt

# RF Architectures & Digital Signal Processing Aspects of Digital Wireless Transceivers has a good treatment of various symbol sync algorithms

def clock_tone_detect(iq, fs_hz = 1.0, clock_tone_search_range_hz = (0.40, 0.50),
                      fine_search_frequency_resolution_hz = 1.0, refine_search=True, debug_plots=False):
    """
    Detect clock tone by squaring method

    :param numpy.ndarray iq: IQ data to process
    :param float fs_hz: Sampling rate in Hz
    :param clock_tone_search_range_hz: 2 element list that specifies the search range for the clock tone
    :param debug_plots:
    :param bool debug_plots: Control debug plots

    :returns:
        - float clock_frequency_offset_hz: Detected clock tone frequency in Hz
        - float clock_phase_offset_rad: Detected clock tone phase in radians
    """
    detailed_debug_plots = False
    try_parabolic_fit = False

    num_samples = int(2*np.floor(len(iq)/2))

    NFFT = len(iq[0:num_samples])
    #f_hz_orig = np.linspace(0, fs_hz / 2, np.int(NFFT / 2), endpoint=False)
    #f_hz = np.linspace(0, fs_hz / 2, np.int(NFFT / 2), endpoint=False)
    f_hz_orig = np.fft.fftfreq(NFFT, 1 / fs_hz)
    f_hz_orig = f_hz_orig[0:np.int(NFFT / 2)]
    f_hz = np.fft.fftfreq(NFFT, 1 / fs_hz)
    f_hz = f_hz[0:np.int(NFFT / 2)]

    # Compute the one-sided spectra of the magnitude of the signal
    iq_abs = np.abs(iq[0:num_samples])
    iq_abs_spectra = np.fft.fft(iq_abs)
    iq_abs_spectra = iq_abs_spectra[0:int(np.round(NFFT/2))]

    # The clock tone spectra can have bowing to the noise floor that can lead to missed detections - flatten it out
    averaging_filter_length = 50
    iq_abs_spectra_avg_flattened = np.abs(iq_abs_spectra) - np.convolve(np.abs(iq_abs_spectra), np.ones((averaging_filter_length)) / averaging_filter_length, mode='same')

    # Remove parts of the spectra that are outside of our search range
    iq_abs_spectra = iq_abs_spectra[(f_hz > clock_tone_search_range_hz[0]) & (f_hz < clock_tone_search_range_hz[1])]
    iq_abs_spectra_avg_flattened = iq_abs_spectra_avg_flattened[(f_hz > clock_tone_search_range_hz[0]) & (f_hz < clock_tone_search_range_hz[1])]
    f_hz = f_hz[(f_hz > clock_tone_search_range_hz[0]) & (f_hz < clock_tone_search_range_hz[1])]

    # Find the index with the highest power, and then do a parabolic fit to get subsample resolution
    index_max = np.argmax(iq_abs_spectra_avg_flattened)

    if detailed_debug_plots:
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(f_hz/1e6, 10*np.log10(np.abs(iq_abs_spectra)), '.-', label='Raw Spectrum')
        plt.plot(f_hz/1e6, 10*np.log10(np.abs(iq_abs_spectra_avg_flattened)), '.-', label='Detrended Spectrum')
        plt.plot(f_hz[index_max]/1e6, 10*np.log10(np.abs(iq_abs_spectra[index_max])), 'x', label='Peak')
        plt.xlabel('Frequency (MHz)')
        plt.ylabel('Clock Tone Amplitude (dB)')
        plt.title('Clock Tone Spectrum (Live Signal)')
        plt.grid()
        plt.legend()
        plt.ylim([-20, 10*np.log10(np.abs(iq_abs_spectra[index_max])) + 5])

    if try_parabolic_fit:
        (frequency_offset_hz_index_fit, frequency_offset_hz_peakval) = parabolic(iq_abs_spectra_avg_flattened, index_max)
        clock_frequency_offset_hz = (f_hz[index_max] + (frequency_offset_hz_index_fit - index_max)*fs_hz/NFFT)

    # Quinn's second estimator (see: https://dspguru.com/dsp/howtos/how-to-interpolate-fft-peak/)
    X = iq_abs_spectra
    k = int(index_max)
    def tau(x):
        return 1/4 * np.log(3*x**2 + 6*x + 1) - np.sqrt(6)/24 * np.log((x + 1 - np.sqrt(2/3)) / (x + 1 + np.sqrt(2/3)))
    ap = (np.real(X[k+1]) * np.real(X[k]) + np.imag(X[k+1]) * np.imag(X[k])) / (np.real(X[k]) * np.real(X[k]) + np.imag(X[k]) * np.imag(X[k]))
    dp = -ap / (1-ap)
    am = (np.real(X[k-1]) * np.real(X[k]) + np.imag(X[k-1]) * np.imag(X[k])) / (np.real(X[k]) * np.real(X[k]) + np.imag(X[k]) * np.imag(X[k]))
    dm = am / (1-am)
    delta_index_max = (dp + dm) / 2 + tau(dp * dp)-tau(dm * dm)
    clock_frequency_offset_hz = f_hz[index_max] + delta_index_max * (fs_hz / NFFT)

    # Compute the DFT at clock_frequency_offset_hz to get an accurate estimate of the phase (because interpolating sucks)
    k = np.argmin(np.abs(f_hz_orig - f_hz[index_max])) + delta_index_max
    N = len(iq_abs)
    X = np.sum(iq_abs * np.exp(-1j * 2 * np.pi * k * np.arange(0, N) / N))
    clock_phase_offset_rad = np.angle(X)

    refine_search = False # This can be very computationally expensive
    if refine_search:
        dft_fine = []
        dft_k_search = np.linspace(np.floor(k) - 1, np.floor(k) + 2, 301)
        for ix, kk in enumerate(dft_k_search):
            dft_fine.append( np.sum(iq_abs * np.exp(-1j * 2 * np.pi * kk * np.arange(0, N) / N)) ) # This line is very computationally expensive
        k_max_index = np.argmax(np.abs(dft_fine))
        k_best_fit_coarse = np.argmin(np.abs(dft_k_search * fs_hz / NFFT-clock_frequency_offset_hz)) # Save for plotting
        clock_frequency_offset_coarse_hz = clock_frequency_offset_hz
        clock_phase_offset_coarse_rad = clock_phase_offset_rad
        clock_frequency_offset_hz = dft_k_search[k_max_index] / NFFT * fs_hz
        clock_phase_offset_rad = np.angle(dft_fine[k_max_index])

    # Interpolate to get the clock tone phase at subsample resolution
    if False:
        clock_phase_offset_rad = np.interp(clock_frequency_offset_hz, f_hz, np.angle(iq_abs_spectra))
        left_indicies = np.linspace(index_max-7, index_max-1, 7, dtype=np.int)
        right_indicies = np.linspace(index_max + 1, index_max + 7, 7, dtype=np.int)
        if np.abs(np.max(np.angle(iq_abs_spectra[left_indicies])) - np.min(np.angle(iq_abs_spectra[left_indicies]))) > 3*np.pi/2:
            phases = np.angle(iq_abs_spectra[left_indicies])
            if np.sum(np.sign(phases)) < 0:
                phases[np.sign(phases)==1] = phases[np.sign(phases)==1] - 2*np.pi
            else:
                phases[np.sign(phases) == -1] = phases[np.sign(phases) == -1] + 2 * np.pi
            left_mean = np.mean(phases)
        else:
            left_mean = np.mean(np.angle(iq_abs_spectra[left_indicies]))
        if np.abs(np.max(np.angle(iq_abs_spectra[right_indicies])) - np.min(np.angle(iq_abs_spectra[right_indicies]))) > 3 * np.pi / 2:
            phases = np.angle(iq_abs_spectra[right_indicies])
            if np.sum(np.sign(phases)) < 0:
                phases[np.sign(phases) == 1] = phases[np.sign(phases) == 1] - 2 * np.pi
            else:
                phases[np.sign(phases) == -1] = phases[np.sign(phases) == -1] + 2 * np.pi
            right_mean = np.mean(phases)
        else:
            right_mean = np.mean(np.angle(iq_abs_spectra[right_indicies]))
        clock_phase_offset_rad = np.interp(clock_frequency_offset_hz, [f_hz[index_max-1], f_hz[index_max+1]] , [left_mean, right_mean])

    if debug_plots:
        plt.figure(figsize=(7.0, 4.75))
        plt.subplot(2, 1, 1)
        plt.plot(f_hz, 10*np.log10(np.abs(iq_abs_spectra_avg_flattened)), label='Clock Tone Spectrum')

        #plt.plot(clock_frequency_offset_hz, 10*np.log10(frequency_offset_hz_peakval), 'x', label='Peak')
        plt.grid(True)
        plt.legend()
        plt.xlabel('Clock Tone Frequency (Hz)')
        plt.ylabel('Power (dB)')
        plt.title('Clock Tone Frequency Detection Results')
        plt.subplot(2, 1, 2)
        plt.plot(f_hz, np.angle(iq_abs_spectra), label='Clock Tone Spectrum')
        plt.plot(clock_frequency_offset_hz, clock_phase_offset_rad, 'x', label='Interpolated Peak')
        plt.xlim(f_hz[index_max-100], f_hz[index_max+100])
        plt.grid(True)
        plt.legend()
        plt.xlabel('Clock Tone Frequency (Hz)')
        plt.ylabel('Clock Time Phase')
        plt.title('Clock Tone Phase Estimation Results')
        plt.tight_layout()

        if refine_search:
            plt.figure()
            plt.subplot(2, 1, 1)
            plt.plot(dft_k_search * fs_hz / NFFT / 1e3, np.abs(dft_fine))
            plt.plot(dft_k_search[k_max_index] * fs_hz / NFFT / 1e3, np.abs(dft_fine[k_max_index]), 'x')
            plt.plot(dft_k_search[k_best_fit_coarse] * fs_hz / NFFT / 1e3, np.abs(dft_fine[k_best_fit_coarse]), 'o')
            plt.xlabel('Clock Frequency (KHz)')
            plt.ylabel('Amplitude')
            plt.title('Fine Clock Tone Recovery (Amplitude)')
            plt.grid()
            ax = plt.gca()
            ax.ticklabel_format(useOffset=False)
            plt.subplot(2, 1, 2)
            plt.plot(dft_k_search * fs_hz / NFFT / 1e3, np.angle(dft_fine))
            plt.plot(dft_k_search[k_max_index] * fs_hz / NFFT / 1e3, np.angle(dft_fine[k_max_index]), 'x')
            plt.plot(dft_k_search[k_best_fit_coarse] * fs_hz / NFFT / 1e3, np.angle(dft_fine[k_best_fit_coarse]), 'o')
            plt.xlabel('Clock Frequency (KHz)')
            plt.ylabel('Phase')
            plt.title('Fine Clock Tone Recovery (Phase)')
            plt.grid()
            ax = plt.gca()
            ax.ticklabel_format(useOffset=False)
            plt.tight_layout()

    return clock_frequency_offset_hz, -1.0 * clock_phase_offset_rad


def parabolic(f, x):
    """Quadratic interpolation for estimating the true position of an
    inter-sample maximum when nearby samples are known.

    f is a vector and x is an index for that vector.

    Returns (vx, vy), the coordinates of the vertex of a parabola that goes
    through point x and its two neighbors.

    Example:
    Defining a vector f with a local maximum at index 3 (= 6), find local
    maximum if points 2, 3, and 4 actually defined a parabola.

    In [3]: f = [2, 3, 1, 6, 4, 2, 3, 1]

    In [4]: parabolic(f, argmax(f))
    Out[4]: (3.2142857142857144, 6.1607142857142856)

    """
    # Requires real division.  Insert float() somewhere to force it?
    xv = 1 / 2 * (f[x - 1] - f[x + 1]) / (f[x - 1] - 2 * f[x] + f[x + 1]) + x
    yv = f[x] - 1 / 4 * (f[x - 1] - f[x + 1]) * (xv - x)

    return (xv, yv)