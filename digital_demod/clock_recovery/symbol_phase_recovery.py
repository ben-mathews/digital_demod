import numpy as np
import matplotlib.pyplot as plt
from dsp_utilities.plotting.scatterplot import scatterplot

def dumb_symbol_phase_recovery(iq_resampled_rrc_filtered, oversample_factor, debug_plots):
    var_metric = []
    for sample_offset in range(0, oversample_factor):
        var_metric.append(np.var(np.abs(iq_resampled_rrc_filtered[(sample_offset)::oversample_factor])))
        if debug_plots:
            scatterplot(iq_resampled_rrc_filtered[(sample_offset):10000:oversample_factor])

    if debug_plots:
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(var_metric)
        plt.xlabel('Offset')
        plt.ylabel('Ratio')
        plt.title('Dump Symbol Phase Offset Detection')
    sample_offset = np.argmin(var_metric)
    return sample_offset