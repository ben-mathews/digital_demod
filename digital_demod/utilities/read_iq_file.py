import numpy as np

def read_iq_file(filename, header_ignore_bytes=0, order='IQ', format=np.int16, max_num_samples=10000):
    """
    Reads IQ data from a binary file
    :param filename: Fully-qualified path and filename of the file to read from
    :param header_ignore_bytes: Number of bytes to skip at beginning of file (default=0)
    :param order:  Order of I and Q data; Valid options = {'IQ', 'QI'}
    :param format: Format of the data; Valid options = {np.int16}
    :param max_num_samples: Maximum number of samples to read from the file

    :return: Numpy array with complex IQ samples

    Example
    iq = read_iq_file('/media/ShareRW/bit_test_srate10_modem10_rfgain15.cs16', header_ignore_bytes=0, order='IQ', format=np.int16, max_num_samples=10000)
    """

    # Check input arguments
    assert order=='IQ' or order=='QI', 'Expecting order to be IQ or QI'
    assert format==np.int16, "Only support data types of numpy.int16"

    # Import from file
    real_samples = np.fromfile(filename, dtype=np.int16, count=int(2*max_num_samples+header_ignore_bytes/np.dtype(format).itemsize))

    # Remove header bytes
    real_samples = real_samples[int(header_ignore_bytes/np.dtype(format).itemsize):]

    # Convert to complex and return
    if order=='IQ':
        iq_samples = real_samples[0::2] + 1j * real_samples[1::2]
    elif order =='QI':
        iq_samples = real_samples[1::2] + 1j * real_samples[0::2]
    else:
        raise Exception('Unhandled data order')

    return iq_samples
