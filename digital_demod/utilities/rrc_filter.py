import numpy as np


def rrc_filter(ntaps, rolloff, fsym_hz, fs_hz):
    """
    Design a RRC filter

    :param int ntaps: Number of filter taps
    :param float rolloff: Filter roll-off (e.g. 0.35)
    :param float fsym_hz: Symbol rate in Hz
    :param float fs_hz: Sample rate in Hz

    :return numpy.ndarray: Filter taps
    """
    time_idx = ((np.arange(ntaps) - ntaps / 2)) / float(fs_hz)
    sample_num = np.arange(ntaps)
    h_rrc = np.zeros(ntaps, dtype=float)

    for x in sample_num:
        t = (x - ntaps / 2) / float(fs_hz)
        if t == 0.0:
            h_rrc[x] = 1.0 - rolloff + (4 * rolloff / np.pi)
        elif rolloff != 0 and t == (1 / fsym_hz) / (4 * rolloff):
            h_rrc[x] = (rolloff / np.sqrt(2)) * (((1 + 2 / np.pi) * \
                                                  (np.sin(np.pi / (4 * rolloff)))) + (
                                                             (1 - 2 / np.pi) * (np.cos(np.pi / (4 * rolloff)))))
        elif rolloff != 0 and np.isclose(np.abs(t), np.abs((1 / fsym_hz) / (4 * rolloff))):
            h_rrc[x] = (rolloff / np.sqrt(2)) * (((1 + 2 / np.pi) * \
                                                  (np.sin(np.pi / (4 * rolloff)))) + (
                                                             (1 - 2 / np.pi) * (np.cos(np.pi / (4 * rolloff)))))
        else:
            Ts = 1 / fsym_hz
            B = rolloff
            h_rrc[x] = (np.sin(np.pi * (t / Ts) * (1 - B)) + (4 * B * (t / Ts)) * np.cos(np.pi * (t / Ts) * (1 + B))) \
                       / (np.pi * (t / Ts) * (1 - (4 * B * (t / Ts)) ** 2))
            h_rrc[x] = (np.sin(np.pi * t * (1 - rolloff) / (1 / fsym_hz)) + \
                        4 * rolloff * (t / (1 / fsym_hz)) * np.cos(np.pi * t * (1 + rolloff) / (1 / fsym_hz))) / \
                       (np.pi * t * (1 - (4 * rolloff * t / (1 / fsym_hz)) ** 2) / (1 / fsym_hz))

    return h_rrc, time_idx