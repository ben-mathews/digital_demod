import os
import time
import copy
import numpy as np
import scipy
import matplotlib
#matplotlib.use("Agg")
import matplotlib.pyplot as plt
from sklearn.cluster import KMeans
#from docx import Document
#from docx.shared import Inches
#from pandas.compat import BytesIO
import io
from digital_demod.utilities.read_iq_file import read_iq_file
from digital_demod.carrier_recovery.carrier_frequency_recovery import carrier_recovery_mfd, carrier_recovery_m_fine, carrier_frequency_shift
from digital_demod.carrier_recovery.carrier_phase_recovery import carrier_phase_recovery, carrier_phase_shift
from digital_demod.carrier_recovery.mod_rec import mod_rec_psk
from digital_demod.clock_recovery.clock_tone_detect import clock_tone_detect
from digital_demod.clock_recovery.symbol_phase_recovery import dumb_symbol_phase_recovery
from digital_demod.measurements.freq_domain_measurements import basic_freq_domain_analysis
from digital_demod.measurements.time_domain_measurements import esn0, symbol_slicer
from digital_demod.signal_utilities.apply_lowpass_filter import apply_lowpass_filter
from digital_demod.signal_utilities.resample_signal import resample_signal
from digital_demod.adaptive_filtering.fractionally_spaced_least_mean_squares import fractionally_spaced_least_mean_squares

from dsp_utilities.file_handlers.iq_file_store import IQFileStore
from dsp_utilities.plotting.filter_visualization import mfreqz, mfreqz_multiple, filter_response
from dsp_utilities.plotting.iq_plot import iq_plot
from dsp_utilities.plotting.scatterplot import scatterplot

from scipy.interpolate.interpolate import lagrange


def digital_demod(iq, fs_hz, mod_order, clock_tone_search_range_hz, file_name=''):
    # Constants to be paramaterized later
    rrc_roll_off_beta = 0.35
    rrc_roll_off_beta_fudge = 0.05
    oversample_factor = 8
    bandwidth_estimation_mode = 'freq_domain_analysis' # 'freq_domain_analysis' or 'estimate_from_predicted_rrc_beta'
    max_carrier_frequency_offset_hz = 0.1 * fs_hz

    debug_plots = False
    debug_plots_detailed = False
    debug_plots_adaptive_filter = False
    generate_report = False

    # If necessary, start the report
    if generate_report:
        document = Document()
        dtitle = 'Demod Results'
        document.sections[0].left_margin = Inches(0.5)
        document.sections[0].right_margin = Inches(0.5)
        document.sections[0].top_margin = Inches(0.5)
        document.sections[0].bottom_margin = Inches(0.5)
        document.add_heading(dtitle, 0)

        thisScriptName = os.path.basename(__file__)
        thisScriptName = os.path.splitext(thisScriptName)[0]
        timeStr = time.strftime("%Y%m%d-%H%M%S")

        document.add_heading('Demodulation Parameters', level=1)
        document.add_paragraph('File Name:\t' + file_name + \
                               '\nNumber of Samples:\t' + str(len(iq)) )

        document.add_heading('Report Generation Script', level=1)
        document.add_paragraph('Name:\t' + thisScriptName + '\nDate:\t' + timeStr)

        document_filename = 'DemodReport_{}.docx'.format(timeStr)
        document.save(document_filename)

    # Create time vector and save off original IQ
    t_sec = np.linspace(0, len(iq) - 1, len(iq)) / fs_hz
    iq_original = copy.deepcopy(iq)  # Save off a copy of the original IQ data

    # Plot data in its initial state
    if debug_plots:
        iq_plot(iq, fs_hz, title_append=' - Raw Capture')

    # Perform modulation recognition
    mod_order = mod_rec_psk(iq=iq, t_sec=t_sec, fs_hz=fs_hz, mod_orders=[2, 4, 8], max_offset_hz=max_carrier_frequency_offset_hz, debug_plots=debug_plots)

    # Coarse carrier frequency offset
    frequency_offset_hz, metric_snr = carrier_recovery_mfd(iq, mod_order, fs_hz, max_carrier_frequency_offset_hz, debug_plots)
    iq = carrier_frequency_shift(iq, -1*frequency_offset_hz, t_sec)
    if debug_plots and debug_plots_detailed:
        iq_plot(iq, fs_hz, title_append=' - After Coarse Carrier Frequency Correction')

    # Fine carrier frequency offset
    frequency_offset_fine_hz, phase_offset_rad = carrier_recovery_m_fine(iq, mod_order, t_sec, debug_plots)
    iq = carrier_frequency_shift(iq, frequency_offset_fine_hz, t_sec)
    if debug_plots and debug_plots_detailed:
        iq_plot(iq, fs_hz, title_append=' - After Fine Carrier Frequency Correction')

    # Carrier phase offset
    phase_offset_rad = carrier_phase_recovery(iq, mod_order, debug_plots)
    iq = carrier_phase_shift(iq, -phase_offset_rad, t_sec)
    phase_offset_rad = carrier_phase_recovery(iq, mod_order, debug_plots)
    iq = carrier_phase_shift(iq, -phase_offset_rad, t_sec)  # Doing it twice helps sometimes with phase wrapping
    if debug_plots:
        iq_plot(iq, fs_hz, title_append=' - After Carrier Frequency and Phase Offset Correction')
        scatterplot(iq)
        plt.gcf().axes[0].set_title(plt.gcf().axes[0].title.get_text() + ' - After Carrier Frequency and Phase Offset Correction')

    # Clock tone detect
    clock_frequency_offset_hz, clock_phase_offset_rad = clock_tone_detect(iq, fs_hz, clock_tone_search_range_hz=clock_tone_search_range_hz, debug_plots=debug_plots)
    clock_frequency_offset_hz = fs_hz-clock_frequency_offset_hz

    if bandwidth_estimation_mode == 'freq_domain_analysis':
        center_frequency_hz, carrier_bandwidth_hz, cn_db = basic_freq_domain_analysis(iq=iq, fs_hz=fs_hz, usable_ibw_fraction=0.8, fraction_power_carrier=0.99, debug_plots=debug_plots)
        carrier_bandwidth_hz = 1.2 * carrier_bandwidth_hz
    elif bandwidth_estimation_mode == 'estimate_from_predicted_rrc_beta':
        carrier_bandwidth_hz = (1.0 + rrc_roll_off_beta_fudge + rrc_roll_off_beta) * clock_frequency_offset_hz
    else:
        assert AssertionError('Unhandled bandwidth estimation mode')
    # Determine coarse bandwidth using frequency-domain technique

    # Low-pass filter the signal based on estimated bandwidth
    iq = apply_lowpass_filter(iq, fs_hz, carrier_bandwidth_hz/2.0)
    if debug_plots and debug_plots_detailed:
        iq_plot(iq=[iq, iq_original], fs_hz=[fs_hz, fs_hz], labels=['Tuned and Filtered', 'Original'], title_append=' - After Filtering')

    # Resample to oversample_factor x the symbol rate
    fs_resampled_hz = oversample_factor * clock_frequency_offset_hz
    iq_resampled = resample_signal(iq, fs_hz, fs_resampled_hz)  # TODO: Resampler doesn't filter out alias very well - look into this later
    t_resampled_sec = np.linspace(0, len(iq_resampled) - 1, len(iq_resampled)) / (fs_resampled_hz)
    if debug_plots:
        iq_plot(iq=[iq_resampled, iq], fs_hz=[fs_resampled_hz, fs_hz], t_sec=[t_resampled_sec, t_sec], labels=['Rsampled', 'Original'], title_append='After Filtering and Resampling to ' + str(oversample_factor) + 'x the Symbol Rate')

    # Design the RRC filter using assumptions about filter's roll-off
    from digital_demod.utilities.rrc_filter import rrc_filter
    rrc_length = 8 * oversample_factor
    h_rrc, time_idx = rrc_filter(rrc_length, rrc_roll_off_beta, clock_frequency_offset_hz, fs_resampled_hz)
    h_rrc = h_rrc / np.sum(h_rrc)

    # RRC filter and remove transients
    iq_resampled_rrc_filtered = np.convolve(iq_resampled, h_rrc)
    iq_resampled_rrc_filtered = iq_resampled_rrc_filtered[int(np.floor(rrc_length / 2)):-(int(np.floor(rrc_length / 2 - 1)))]

    if debug_plots:
        iq_plot(iq=[iq_resampled_rrc_filtered, iq_resampled], fs_hz=[fs_resampled_hz, fs_resampled_hz], t_sec=[t_resampled_sec, t_resampled_sec], labels=['RRC Filtered', 'Original Resampled'], title_append=' - Before and After RRC Filtering')

    # Recover symbol phase - this is about the second stupidest possible way of doing this - figure something smarter out
    sample_offset = dumb_symbol_phase_recovery(iq_resampled_rrc_filtered, oversample_factor, debug_plots)
    # At this point we subsequent iq vectors no longer start at t_sec=0
    iq_resampled_rrc_filtered = iq_resampled_rrc_filtered[sample_offset:]
    t_resampled_sec = t_resampled_sec[sample_offset:]
    symbols = iq_resampled_rrc_filtered[0::oversample_factor]
    t_symbols_sec = t_resampled_sec[sample_offset:]
    t_symbols_sec = t_symbols_sec[0::oversample_factor]

    # Correct the phase again
    if True:
        phase_offset_resampled_rad = carrier_phase_recovery(symbols, mod_order, debug_plots)
        phase_offset_resampled_rad = phase_offset_resampled_rad
        symbols = carrier_phase_shift(symbols, -phase_offset_resampled_rad, t_symbols_sec)
        iq_resampled_rrc_filtered = carrier_phase_shift(iq_resampled_rrc_filtered, -phase_offset_resampled_rad, t_resampled_sec)
        iq_resampled = carrier_phase_shift(iq_resampled, -phase_offset_resampled_rad, t_resampled_sec)

    # Run another mod req and make sure we get consistent results
    if False:
        new_mod_order = mod_rec_psk(iq=iq_resampled_rrc_filtered, t_sec=t_resampled_sec, fs_hz=fs_resampled_hz, mod_orders=[2, 4, 8], max_offset_hz=max_carrier_frequency_offset_hz, debug_plots=debug_plots)
        if mod_order != new_mod_order:
            raise AssertionError('Mod order changed')

    # Extract constellation
    kmeans = KMeans(n_clusters=mod_order)
    symbols_real = np.array(list(zip(np.real(symbols), np.imag(symbols))), dtype=np.float32)
    mapping = kmeans.fit_predict(symbols_real)
    constellation = []
    for point in kmeans.cluster_centers_:
        constellation.append(point[0] + 1j * point[1])
    if debug_plots_detailed:
        scatterplot(iq=[symbols, np.asarray(constellation)], labels=['Raw Symbols', 'Detected Points'])
    constellation = np.array(constellation)
    #mapping = kmeans.predict(symbols_real)
    desired_signal = constellation[mapping]
    if False:
        iq_plot(iq=[symbols, desired_signal])

    # Scale the data
    scale_factor = np.mean(np.abs(constellation))
    constellation = constellation / scale_factor
    iq_resampled = iq_resampled / scale_factor
    desired_signal = desired_signal / scale_factor
    symbols = symbols / scale_factor

    # Rotate QPSK
    if mod_order == 4:
        constellation = constellation * np.exp(1j * np.pi / 4.0)
        iq_resampled = iq_resampled * np.exp(1j * np.pi / 4.0)
        desired_signal = desired_signal * np.exp(1j * np.pi / 4.0)
        symbols = symbols * np.exp(1j * np.pi / 4.0)

    if debug_plots:
        scatterplot(symbols)
        plt.title('Constellation - After RRC Filtering')
        for point in constellation:
            plt.plot(np.real(point), np.imag(point), 'kx')

        plt.figure()
        plt.subplot(2, 1, 1)
        plt.plot(np.real(symbols - desired_signal))
        plt.grid(True)
        plt.xlabel('Symbol Number')
        plt.xlabel('Magitude Error (I)')
        plt.title('Symbol Error (I)')
        plt.subplot(2, 1, 2)
        plt.plot(np.imag(symbols - desired_signal))
        plt.grid(True)
        plt.xlabel('Symbol Number')
        plt.xlabel('Magitude Error (Q)')
        plt.title('Symbol Error (Q)')
        plt.tight_layout()

    # Fractionally spaced equalizer
    u = copy.deepcopy(iq_resampled[sample_offset::])
    u = u[0:int(oversample_factor * np.floor(len(u) / oversample_factor) + 1)]
    d = copy.deepcopy(desired_signal[0:])
    d = d[np.int(len(h_rrc) / oversample_factor / 2)::]
    step = 5e-3
    y, e, w = fractionally_spaced_least_mean_squares(u[0:int(oversample_factor*100)], d, step, h_rrc, oversample_factor)
    y, e, w = fractionally_spaced_least_mean_squares(u, d, step, w[:,-1], oversample_factor)
    #TODO: Figure out how run data back through the filter backwards

    if debug_plots_adaptive_filter:
        plt.figure()
        plt.subplot(3, 1, 1)
        plt.plot(np.transpose(np.real(w)))
        plt.xlabel('Filter Iteration Number')
        plt.ylabel('Filter Tap Values')
        plt.title('Real Filter Taps')
        plt.subplot(3, 1, 2)
        plt.plot(np.transpose(np.imag(w)))
        plt.xlabel('Filter Iteration Number')
        plt.ylabel('Filter Tap Values')
        plt.title('Imag Filter Taps')
        plt.subplot(3, 1, 3)
        # plt.plot(np.real(e), label='Real')
        # plt.plot(np.imag(e), label='Imag')
        plt.plot(np.abs(e), label='Abs')
        plt.xlabel('Filter Iteration Number')
        plt.ylabel('Error')
        plt.title('Filter Error')
        plt.tight_layout()
    w_final = w[:, -1]

    if debug_plots:
        mfreqz_multiple(b=[h_rrc, w_final], a=[1, 1], labels=['RRC', 'Adaptive EQ'])
        plt.gcf().axes[0].set_title('RRC and Adaptive EQ Magnitude Response')
        plt.gcf().axes[1].set_title('RRC and Adaptive EQ Phase Response')

        filter_iterations = [int(0.00 * np.shape(w)[1]), int(0.25 * np.shape(w)[1]), int(0.50 * np.shape(w)[1]), int(0.75 * np.shape(w)[1]), int(1.0 * np.shape(w)[1]-1)]
        for filter_iteration in filter_iterations:
            filter_response(taps=[h_rrc, w[:, filter_iteration]], labels=['RRC', 'Adaptive Eq'])
            plt.gcf().axes[0].set_title(plt.gcf().axes[0].title.get_text() + ' ({0}/{1} iterations)'.format(filter_iteration, np.shape(w)[1]))
            plt.gcf().axes[1].set_title(plt.gcf().axes[1].title.get_text() + ' ({0}/{1} iterations)'.format(filter_iteration, np.shape(w)[1]))

        filter_response(taps=[h_rrc, w_final], labels=['RRC', 'Final Adaptive Eq'])

        # Debug symbol clock drift
        from carrier_recovery.carrier_frequency_recovery import parabolic
        center_tap = []
        for idx in np.arange(0, np.shape(w)[1], 1):
            pfit = parabolic(np.real(w[:, idx]), np.argmax(np.real(w[:, idx])))
            center_tap.append(pfit[0])
        plt.figure(figsize=(7.0, 4.75))
        plt.plot(center_tap)
        plt.grid()
        plt.xlabel('Filter Iteration')
        plt.ylabel('Adaptive Filter Center Tap')
        plt.title('Adaptive Filter Center Tap')

    output_mode = 'Use Adaptive Filter Output'
    if output_mode == 'Use Adaptive Filter Output':
        symbols_final = y
    elif output_mode == 'Resample Using Final Filter':
        iq_resampled_adaptive_filtered = np.convolve(iq_resampled, w_final)
        iq_resampled_adaptive_filtered = iq_resampled_adaptive_filtered[
                                         int(np.floor(rrc_length / 2)):-(int(np.floor(rrc_length / 2 - 1)))]
        sample_offset = dumb_symbol_phase_recovery(iq_resampled_adaptive_filtered, oversample_factor, debug_plots)
        iq_resampled_adaptive_filtered = iq_resampled_adaptive_filtered[sample_offset:]
        symbols_final = iq_resampled_adaptive_filtered[0::oversample_factor]

    # Compute Es/N0
    symbols_truth = desired_signal[np.int(len(h_rrc) / oversample_factor / 2):np.int(len(h_rrc) / oversample_factor / 2)+len(symbols_final)]
    es_n0_raw_dB = esn0(symbols, constellation[symbol_slicer(symbols, constellation)])
    es_n0_equalized_dB = esn0(symbols_final, symbols_truth)

    if debug_plots:
        fig = scatterplot([symbols, symbols_final], ['Raw Symbols (Es/N0 = {0:2.1f} dB)'.format(es_n0_raw_dB), 'Equalized Symbols (Es/N0 = {0:2.1f} dB)'.format(es_n0_equalized_dB)])
        fig.axes[0].plot(np.real(constellation), np.imag(constellation), 'kx', label='Ideal Symbols')
        fig.axes[0].legend()
        fig.axes[0].set_title('Final Constellation - After Adaptive Equalization')


    if generate_report:
        for i in plt.get_fignums():
            plt.figure(i)
            memfile = BytesIO()
            plt.savefig(memfile)
            document.add_picture(memfile,)
            if not debug_plots:
                plt.close()
        document.save(document_filename)





    # Begin analysis
    if False:
        from scipy.signal import upfirdn
        original_signal = copy.deepcopy(u)
        reference_signal = upfirdn(w_final, desired_signal, oversample_factor, 1)
        reference_signal = reference_signal[int(len(w_final) / 2 + oversample_factor)::]
        print(len(reference_signal))
        reference_signal = reference_signal[0:len(original_signal)]

        sample_offset1 = dumb_symbol_phase_recovery(reference_signal, oversample_factor, True)
        reference_signal = reference_signal[sample_offset1::]
        sample_offset2 = dumb_symbol_phase_recovery(original_signal, oversample_factor, True)
        original_signal = original_signal[sample_offset2::]
        original_signal = original_signal[1:len(reference_signal)]
        reference_signal = reference_signal[0:len(original_signal)]

        scaling_factor = np.mean(np.abs(original_signal[0::oversample_factor] / reference_signal[0::oversample_factor]))
        reference_signal = scaling_factor * reference_signal

        residual = original_signal - reference_signal
        iq_plot([original_signal, residual])

        iq_plot([original_signal, reference_signal])
    # End analysis

    put_a_breakpoint_here = 1




def dist(a, b, ax=1):
    return np.linalg.norm(a - b, axis=ax)


if __name__ == '__main__':
    mod_order = 8
    fs_hz = 1.0

    if True:
        filename = '/home/ben/dev/gitlab/digital_demod/output.bin'

        iq_file_store = IQFileStore(filename=filename)
        t_start_actual_sec, t_end_actual_sec = iq_file_store.read_file(t_start_sec=iq_file_store.t_start_sec, t_end_sec=np.min([iq_file_store.t_start_sec+0.25, iq_file_store.t_end_sec]))
        iq = iq_file_store.iq_file.iq
        fs_hz = iq_file_store.iq_file.fs_hz
        clock_tone_search_range_hz = (0.10 * fs_hz, 0.50 * fs_hz)
        mod_order = 4

    digital_demod(iq, fs_hz, mod_order, clock_tone_search_range_hz, filename)

    """
    if False:
        for offset in range(0,oversample_factor):
            scatterplot(iq_resampled[(offset)::oversample_factor])
            plt.title('Offset = ' + str(offset))

    if False:
        iq_resampled_rrc_filtered = iq_resampled_rrc_filtered[:int(oversample_factor * np.floor(len(iq_resampled_rrc_filtered)/oversample_factor))]
        iq_resampled = iq_resampled[sample_offset:]
        iq_resampled = iq_resampled[:int(oversample_factor * np.floor(len(iq_resampled) / oversample_factor))]

    if False:
        for sample_offset in range(0,oversample_factor):
            scatterplot(y[(sample_offset)::oversample_factor])
            plt.title('Offset = ' + str(sample_offset))            
    """