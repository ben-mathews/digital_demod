# digital_demod

A simple Python-based digital demodulation suite for processing recorded IQ files.

This project is a collection of functions that can be combined to perform basic demodulation, analysis, plotting, etc., of digitally-modulated signals, primarily SATCOM signals.  

My intent is publishing this project is two-fold:

1. Throughout my career as I have re-written the same basic functions over and over as I have changed jobs, projects, languages, etc.  While most frameworks provide basic functional building blocks, it seems like I always end up having to write additional code to wrap these in the same ways to get the functionality that I always need.  At this point in my career I'd like to stop reinventing the wheel, so I am writing this on the side and publishing to my personal repo to make it easier to reuse in the future.  

2. This project is also intended as a proof-of-concept tool to support learning about basic concepts in digital communications and signal demodulation.  The code in this project is intended to be verbose and easy to read, and is meant to non-expert enable users to gain an intuitive understanding of these concepts (rather than a theoretically-driven understanding).  I make no use of DSP tricks-of-the-trade or optimizations that may have the side effect of obfuscating the intention of the code.  

These algorithms are completely unoptimized for performance in either a theoretical or computational sense.  If you're looking for highly-optimized, production-ready code, look elsewhere.  The focus is going from raw, unsynchronized IQ samples captured from a low-cost SDR to soft symbols.  This project does not implement Forward Error Correction (FEC) processing, bit synchronization, framing, etc.  There are other projects that offer this functionality (e.g. gnuradio).  

The main function is digital_demod.py, which implements the signal processing show in the figure below.  Data is read from a file of interleaved 16 bit I and Q samples.  Modulation recognition is optionally performed, followed by carrier frequency and phase recovery, and symbol rate detection.  The data is then resampled to N times the symbol rate, and RRC filtered to get symbol estimates.  These are used with the resampled IQ data and an adaptive equalizer to compute final symbol estimates.

![alt text](doc/images/basic_diagram.png "Logo Title Text 1")  

Virtual Environment Creation:
For Python 3,
```bash
sudo apt install python3-tk
python3 -m venv --copies ~/dev/venvs/venv-dsp-20190510
source   ~/dev/venvs/venv-dsp-20190510/bin/activate
pip install numpy matplotlib scipy pandas sklearn python-docx 
```
For Python 2,
```bash
sudo apt-get install python-tk
sudo apt-get install python-dev # Need this for subprocess32
virtualenv --no-site-packages ~/dev/venvs/venv-python2-dsp-20190510
source ~/dev/venvs/venv-python2-dsp-20190510/bin/activate
pip install numpy matplotlib scipy pandas sklearn docx 

```